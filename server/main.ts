import { Meteor } from 'meteor/meteor';


import "../imports/api/server/restaurant_categories_methods"
import { generateDefaultRestaurantCategories } from '../imports/services/restaurant_categories_service'
import { generateDefaultUser } from '/imports/services/users_service';

Meteor.startup(() => {
  console.log("SERVER - Server launched")

  // Generate default data
  generateDefaultRestaurantCategories()
  generateDefaultUser()
});
