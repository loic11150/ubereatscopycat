import { Meteor } from 'meteor/meteor'
import React, { useEffect, useState } from 'react'
import { RestaurantCategory } from '/imports/types'

interface Props {

}

const RestaurantCategoriesDisplayer : React.FC<Props> = (props : Props) => {
    const [categories, setCategories] = useState<Array<RestaurantCategory>>([])
    
    useEffect(() => {

        Meteor.call('restaurant_categories.getAll',null, ((error, data) => {
            if(error){
                console.log(error)
                return
            }
            setCategories(data)
        }))
    }, [])
    
    return(
        <div className="flex justify-between">
            {categories &&
                <>
                    {categories.map(cat => {
                        return(<div key={cat._id} className="category">
                            {cat.name}
                        </div>)
                    })}
                </>
            }
        </div>
    )
}

export default RestaurantCategoriesDisplayer